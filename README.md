# Guimauve & Moutarde

Projet Hackathon de l'équipe 'Guimauve & Moutarde'

## Installation

* Installer node et npm
* Installer Angular CLI - `npm install -g @angular/cli` - [Documentation](https://angular.io/cli)
* Lancer `npm install`

## Development server

Lancer `ng serve` pour démarrer le serveur. Aller à l'adresse `http://localhost:4200/`.
L'application se mettra à jour automatiquement.
